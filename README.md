This project is intended to test the "laggy scroll" issue in Unity on Android, 
which is linked to an issue with an input system (either the platform or Unity, or a combination of both).

Do not forget to select the input system you are interested in in 
`Project Settings -> Player -> Android -> Other Settings -> Active Input Handling` before building.

There are two folders in `Results` folder: `Old System` and `New System`. One for each Unity input system. Each of them contains:
- `apk` file used for testing
- Log file from the testing
- Screen recording of the testing
- Graphs for every swipe in the log

If you want to create same graphs for your logs or reproduce graphs from the repository, 
just run the `graphs.py` Python script from `Results` folder with arguments `--log`, `--output-dir` and `--comment` (optional).
Something like this: 
```
py graphs.py --log="path/to/log.log" --output-dir="directory/to/save/graphs" --comment="Some comment to attach to the graphs title"
```