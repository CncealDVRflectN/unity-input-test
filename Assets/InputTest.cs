using System;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem.EnhancedTouch;
using UnityEngine.InputSystem.UI;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;
#endif

public class InputTest : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private readonly StringBuilder _builder = new();

    private DateTime? _dragStartTime;
    private DateTime? _lastDragTime;
#if ENABLE_INPUT_SYSTEM
    private double? _updateTouchLastTime;
#endif


    private void Awake()
    {
        Application.targetFrameRate = 60;

        GameObject eventsRoot = new("EventSystem");
        eventsRoot.AddComponent<EventSystem>();

#if ENABLE_INPUT_SYSTEM
        eventsRoot.AddComponent<InputSystemUIInputModule>();
        EnhancedTouchSupport.Enable();
#else
        eventsRoot.AddComponent<StandaloneInputModule>();
#endif
    }

    private void Update()
    {
        Context context = new() { Placement = "Update" };

#if ENABLE_INPUT_SYSTEM
        if (Touch.activeTouches.Count > 0)
        {
            Touch touch = Touch.activeTouches[0];
            float deltaTime = (_updateTouchLastTime.HasValue) ?
                (float)(touch.time - _updateTouchLastTime.Value) :
                (float)(touch.time - touch.startTime);
            context.TouchInfo = new()
            {
                DeltaTime = deltaTime * 1000f,
                DeltaPos = touch.delta
            };

            _updateTouchLastTime = touch.time;
        }
        else
        {
            _updateTouchLastTime = null;
        }
#else
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            context.TouchInfo = new()
            {
                DeltaTime = touch.deltaTime * 1000f,
                DeltaPos = touch.deltaPosition
            };
        }
#endif

        PrintLog(context);
    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        _dragStartTime = DateTime.Now;

        Context context = new() { Placement = "OnBeginDrag" };
        context.DragInfo = new()
        {
            StartTime = _dragStartTime.Value,
            DeltaPos = eventData.delta
        };

        PrintLog(context);
    }

    public void OnDrag(PointerEventData eventData) 
    {
        Context context = new() { Placement = "OnDrag" };
        context.DragInfo = new()
        {
            StartTime = _dragStartTime.Value,
            DeltaPos = eventData.delta,
            LastDragTime = _lastDragTime,
        };

        PrintLog(context);

        _lastDragTime = DateTime.Now;
    }

    public void OnEndDrag(PointerEventData eventData) 
    {
        Context context = new() { Placement = "OnEndDrag" };
        context.DragInfo = new()
        {
            StartTime = _dragStartTime.Value,
            DeltaPos = eventData.delta,
            LastDragTime = _lastDragTime
        };

        PrintLog(context);

        _dragStartTime = null;
        _lastDragTime = null;
    }


    private void PrintLog(Context context)
    {
        _builder.Append("INPUT TEST ")
            .AppendLine(context.Placement)
            .Append("Frame: ")
            .AppendLine(Time.frameCount.ToString())
            .Append("Frame delta time: ")
            .Append(Time.deltaTime * 1000f)
            .AppendLine(" ms");

        if (context.TouchInfo.HasValue)
        {
            _builder.Append("Touch delta time: ")
                .Append(context.TouchInfo.Value.DeltaTime)
                .AppendLine(" ms")
                .Append("Touch delta position: ")
                .AppendLine(context.TouchInfo.Value.DeltaPos.ToString());
        }

        if (context.DragInfo.HasValue)
        {
            DateTime time = DateTime.Now;

            if (context.DragInfo.Value.LastDragTime.HasValue)
            {
                TimeSpan dragTimeDelta = time - context.DragInfo.Value.LastDragTime.Value;
                _builder.Append("Drag delta time: ")
                    .Append(dragTimeDelta.TotalMilliseconds)
                    .AppendLine(" ms");
            }

            _builder.Append("Drag delta position: ")
                .AppendLine(context.DragInfo.Value.DeltaPos.ToString());
        }

        Debug.Log(_builder.ToString());
        _builder.Clear();
    }


    private struct Context
    {
        public string Placement;
        public TouchInfo? TouchInfo;
        public DragInfo? DragInfo;
    }

    private struct TouchInfo
    {
        public float DeltaTime;
        public Vector2 DeltaPos;
    }

    private struct DragInfo
    {
        public DateTime StartTime;
        public Vector2 DeltaPos;
        public DateTime? LastDragTime;
    }
}
