import argparse
import os
import re
import matplotlib.pyplot as plot
import matplotlib.gridspec as gridspec


class FrameData:
    frame: int
    markers: set[str]
    frame_delta_time: float
    touch_delta_time: float
    drag_delta_time: float

    def __init__(self):
        self.frame = None
        self.markers = set()
        self.frame_delta_time = None
        self.touch_delta_time = None
        self.drag_delta_time = None

    def __str__(self):
        return f"Frame {self.frame}\nMarkers: {self.markers}\nDelta time: {self.frame_delta_time} ms\nTouch delta time: {self.touch_delta_time} ms\nDrag delta time: {self.drag_delta_time} ms"
    

class SwipeData:
    frames: list[FrameData]

    def __init__(self):
        self.frames = list()


marker_regex = re.compile(r"INPUT TEST (?P<marker>\w+)")
def parse_marker(line: str) -> str:
    marker_match = marker_regex.search(line)
    if marker_match:
        return marker_match.group("marker")
    else:
        return None


frame_regex = re.compile(r"Frame: (?P<frame>\d+)")
def parse_frame_number(line: str) -> int:
    frame_match = frame_regex.search(line)
    if frame_match:
        return int(frame_match.group("frame"))
    else:
        return None
    

frame_delta_regex = re.compile(r"Frame delta time: (?P<delta>\d+([.,]\d+)*)")
def parse_frame_delta_time(line: str) -> float:
    delta_match = frame_delta_regex.search(line)
    if delta_match:
        return float(delta_match.group("delta"))
    else:
        return None
    

touch_delta_regex = re.compile(r"Touch delta time: (?P<delta>\d+([.,]\d+)*)")
def parse_touch_delta_time(line: str) -> float:
    delta_match = touch_delta_regex.search(line)
    if delta_match:
        return float(delta_match.group("delta"))
    else:
        return None
    

drag_delta_regex = re.compile(r"Drag delta time: (?P<delta>\d+([.,]\d+)*)")
def parse_drag_delta_time(line: str) -> float:
    delta_match = drag_delta_regex.search(line)
    if delta_match:
        return float(delta_match.group("delta"))
    else:
        return None


def parse_log(path: str) -> list[FrameData]:
    frames = []

    with open(path, "r") as file:
        active_marker: str = None
        frame: FrameData = None
        for line in file:
            frame_number = parse_frame_number(line)
            if frame_number:
                if not frame or frame.frame != frame_number:
                    frame = FrameData()
                    frame.frame = frame_number
                    frames.append(frame)
                
                if active_marker:
                    frame.markers.add(active_marker)
                    active_marker = None

            marker = parse_marker(line)
            if not active_marker: active_marker = marker
            
            frame_delta_time = parse_frame_delta_time(line)
            if frame_delta_time: frame.frame_delta_time = frame_delta_time

            touch_delta_time = parse_touch_delta_time(line)
            if touch_delta_time: frame.touch_delta_time = touch_delta_time

            drag_delta_time = parse_drag_delta_time(line)
            if drag_delta_time: frame.drag_delta_time = drag_delta_time

    return frames


def get_swipes(frames: list[FrameData]) -> list[SwipeData]:
    swipes = []
    swipe: SwipeData = None
    for frame in frames:
        if swipe:
            swipe.frames.append(frame)
            if "OnEndDrag" in frame.markers:
                swipe = None
        elif "OnBeginDrag" in frame.markers:
            swipe = SwipeData()
            swipe.frames.append(frame)
            swipes.append(swipe)

    return swipes


def plot_time_deltas_scatter(frames: list[FrameData], plot: plot.Axes):
    plot.set_xlabel("Frame")
    plot.set_ylabel("Time delta (ms)")

    frame_numbers = list()
    frame_deltas = list()
    touch_deltas = list()
    drag_deltas = list()
    for frame in frames:
        frame_numbers.append(frame.frame)
        frame_deltas.append(frame.frame_delta_time)
        touch_deltas.append(frame.touch_delta_time)
        drag_deltas.append(frame.drag_delta_time)

    plot.scatter(x=frame_numbers, y=frame_deltas, s=10, label="Frame delta time")
    plot.scatter(x=frame_numbers, y=touch_deltas, s=10, label="Touch delta time")
    plot.scatter(x=frame_numbers, y=drag_deltas, s=10, label="Drag delta time")
    plot.legend()


def plot_time_deltas_boxes(frames: list[FrameData], plot: plot.Axes):
    plot.set_ylabel("Time delta (ms)")
    plot.set_xticklabels(["Frame delta time", "Touch delta time", "Drag delta time"])

    frame_deltas = list()
    touch_deltas = list()
    drag_deltas = list()
    for frame in frames:
        if frame.frame_delta_time: frame_deltas.append(frame.frame_delta_time)
        if frame.touch_delta_time: touch_deltas.append(frame.touch_delta_time)
        if frame.drag_delta_time: drag_deltas.append(frame.drag_delta_time)

    plot.boxplot([frame_deltas, touch_deltas, drag_deltas])


def plot_drag_deltas_pie(frames: list[FrameData], plot: plot.Axes):
    plot.set_title("Drag time deltas")

    low_count = 0
    middle_count = 0
    high_count = 0
    for frame in frames:
        if frame.drag_delta_time:
            if frame.drag_delta_time < 10.0:
                low_count += 1
            elif frame.drag_delta_time < 20.0:
                middle_count += 1
            else:
                high_count += 1

    values = list()
    labels = list()
    colors = list()
    
    if low_count > 0:
        values.append(low_count)
        labels.append("< 10 ms")
        colors.append("#6CA0DC")
    
    if middle_count > 0:
        values.append(middle_count)
        labels.append("< 20 ms")
        colors.append("#77DD77")

    if high_count > 0:
        values.append(high_count)
        labels.append("≥ 20 ms")
        colors.append("#FF6961")

    plot.pie(values, labels=labels, colors=colors, autopct="%1.1f%%", pctdistance=1.25, labeldistance=0.4)


def plot_swipes_data(swipes: list[SwipeData], comment: str, output_dir: str):
    figure_title = "Time deltas"
    if comment:
        figure_title += f" ({comment})"

    for index, swipe in enumerate(swipes):
        figure = plot.figure(figsize=(20, 10))
        figure.suptitle(figure_title)
        grid_specs = gridspec.GridSpec(2, 3)
        plot_time_deltas_scatter(swipe.frames, figure.add_subplot(grid_specs[0, 0:3]))
        plot_time_deltas_boxes(swipe.frames, figure.add_subplot(grid_specs[1, 0:2]))
        plot_drag_deltas_pie(swipe.frames, figure.add_subplot(grid_specs[1, 2:3]))
        figure.savefig(os.path.join(output_dir, f"swipe_{index}.png"), dpi=300)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--log", type=str, help="Path to log")
    parser.add_argument("--output-dir", type=str, help="Output directory")
    parser.add_argument("--comment", type=str, help="Comment to add to the plots title")
    args = parser.parse_args()
    frames = parse_log(args.log)
    swipes = get_swipes(frames)
    plot_swipes_data(swipes, args.comment, args.output_dir)


if __name__ == "__main__":
    main()